<%@ page import="java.util.Map" %>
<%@ page import="java.util.Iterator" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <meta charset=\"UTF-8\">
    <title>Server response</title>
</head>
<body>
<h1>Server side response</h1>
<p>Thanks for your submission. The values sent to the server are as follows:</p>

<%
    Map<String, String[]> map = request.getParameterMap();
    Iterator<Map.Entry<String, String[]>> i = map.entrySet().iterator(); %>


    <table style = 'border: 1px solid black'><tr>

  <%  while(i.hasNext()) {
        Map.Entry<String, String[]> entry = i.next();
        String key = entry.getKey(); //.toUpperCase();
        String[] values = entry.getValue();

        if(key.contains("submit") || key.contains("button")) {
            continue;
        }

        int index = key.indexOf("[]");
        if(index != -1) {
            key = key.substring(0, index);
        } %>

        <td style = 'border: 1px solid black'> <%= key%> </td>

        <% for(String value: values) { %>
            <td style = 'border: 1px solid black'> <%= value%> </td>
        <% } %>

        </tr>
    <% } %>
    </table>

    </body></html>

</body>

</html>

