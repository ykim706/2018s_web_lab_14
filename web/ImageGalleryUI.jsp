<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
<body><%=request.getAttribute("photosFolder")%>


<table>
    <tr>
    <th>Image</th>
        <th><a href="/ImageGalleryDisplay?sortColumn=filename&order=${filenameSortToggle}ending">File Name
        <img src="images/sort-${filenameSortToggle}.png"></a></th>
        <th><a href="/ImageGalleryDisplay?sortColumn=filesize&order=${filesizeSortToggle}ending">File Size
            <img src="images/sort-${filesizeSortToggle}.png"></a></th>

    </tr>
    <c:forEach items="${fileDataList}" var="filedatalist">
        <tr>

            <td><img src="/Photos/${filedatalist.thumbPath.name}"></td>
            <td>${filedatalist.thumbDisplay}</td>
            <td>${filedatalist.fullfileSize}</td>
        </tr>
    </c:forEach>
</table>

</body>
</head>
</html>
